(defproject clojure_proj "0.1.0-SNAPSHOT"
  :description "SICP Workbook"
  :url "http://www.louisknapp.com"
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main src.core)
