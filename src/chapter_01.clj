(ns chapter_01
   (:gen-class))

(def a 3)

(def b (+ a 1))

(defn sum-of-larger-squares [a b c]
  (cond
		(and (< a b) (< a c)) (+ (* b b) (* c c))
		(and (< b c) (< b a)) (+ (* a a) (* c c))
		(and (< c b) (< c a)) (+ (* a a) (* b b))
))
