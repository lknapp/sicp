(ns chapter_01-test
  (:require [clojure.test :refer :all]
            [chapter_01 :refer :all]))

(deftest test_1.1
  (testing "1.1"
    (is (= 10 10))
    (is (= (+ 5 3 4) 12))
    (is (= (- 9 1) 8))
    (is (= (/ 6 2) 3))
    (is (= (+ (* 2 4) (- 4 6)) 6))
    (is (= (+ a b (* a b)) 19))
    (is (= (= a b) false))
    (is (= (if (and (> b a) (< b (* a b))) b a) 4))
    (is (=
         (cond
           (= a 4) 6
           (= b 4) (+ 7 6 a)
           :else 25
           )
         16))
    (is (=
         (+ 2 (if (> b a) b a))
         6))
    (is (=
         (* (cond (> a b) a
                  (< a b) b
                  :else -1)
            (+ a 1))
         16))))

(deftest test_1.2
  (testing "1.2"
    (is (=
         (/ (+ 5 4 (- 2 (- 3 (+ 6 (/ 1 5))))) (* 3 (- 6 2) (- 2 7)))
         (/ -71 300)
         ))))

(deftest test_1.3
  (testing "1.3"
    (is (= (sum-of-larger-squares 1 2 3) 13))
    (is (= (sum-of-larger-squares 5 4 6) 61))
    ))
